package main;
import java.io.*;

import com.stagiu.framework.clients.Clients;
import com.stagiu.framework.room.Room;

public class Main {

	public static void main(String[] args) {
		String floor = null;
		String price = null;
		String status = null;
		String endowments = null;
		String roomNumber = null;
		Integer rebuild = 1;
		while (rebuild == 1) {
			BufferedReader bufferRead = new BufferedReader(
					new InputStreamReader(System.in));
			try {
				System.out.println("Press 1 - to build a room and 0-Exit");
				String option = bufferRead.readLine();
				rebuild = Integer.parseInt(option);
				System.out.println("Please enter the room floor");
				floor = bufferRead.readLine();
				System.out.println("Please enter the room's number");
				roomNumber = bufferRead.readLine();
				System.out.println("Please enter the room's endowments");
				endowments = bufferRead.readLine();
				System.out.println("Please enter the room's status");
				status = bufferRead.readLine();
				System.out.println("Please enter the room's price");
				price = bufferRead.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			Room room = new Room(status, endowments, Double.parseDouble(price),
					Integer.parseInt(floor), Integer.parseInt(roomNumber));
		
			/**
		 * 
		 */

			Integer reenterClient = 1;
			while (reenterClient == 1) {
				String name = null;
				String CNP = null;
				String loyalty = null;
				String age = null;
				String specialSituation = null;
				/**
		 * 
		 */

				try {
					System.out
							.println("Press 1 - to build a client and 0-Exit");
					String option = bufferRead.readLine();
					reenterClient = Integer.parseInt(option);
					System.out.println("Please enter clients name ");
					name = bufferRead.readLine();
					System.out.println("Please enter clients age ");
					age = bufferRead.readLine();
					System.out.println("Please enter clients CNP");
					CNP = bufferRead.readLine();
					System.out.println("Please enter clients loyality option");
					loyalty = bufferRead.readLine();
					System.out
							.println("Please enter clients special situation");
					specialSituation = bufferRead.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
				Clients client = new Clients(name, Double.parseDouble(CNP),
						loyalty, Integer.parseInt(age), specialSituation);
				client.setName(name);
				client.setAge(Integer.parseInt(age));
				client.setCNP(Double.parseDouble(CNP));
				client.setLoyalty(loyalty);
				client.setSpecialSituation(specialSituation);
				client.displayClient(client);
			}
		}
	}
}
