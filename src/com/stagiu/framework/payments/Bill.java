package com.stagiu.framework.payments;
import java.util.GregorianCalendar;
import com.stagiu.framework.room.Room;
import com.stagiu.framework.clients.Clients;
public class Bill {
	private Clients client;
	private Room room;
	private GregorianCalendar dateOfArrival;
	private GregorianCalendar departureDate;
	/**
	 * 
	 * @param client
	 * @param room
	 * @param dateOfArrival
	 * @param departureDate
	 */
	public  Bill(Clients client,Room room,GregorianCalendar dateOfArrival,GregorianCalendar departureDate ){
		this.client=client;
		this.room=room;
		this.dateOfArrival=dateOfArrival;
		this.departureDate=departureDate;
		
	}
	
	
	
	
	/**
	 * 
	 * @param dateOfArrival
	 */
	public void setDateOfArrival(GregorianCalendar dateOfArrival){
		this.dateOfArrival=dateOfArrival;
	}
	/**
	 * 
	 * @param dateOfArrival
	 * @return
	 */
	public GregorianCalendar getDateOfArrival(GregorianCalendar dateOfArrival){
		return dateOfArrival;
	}
	/**
	 * 
	 * @param departureDate
	 */
	public void setDepartureDate(GregorianCalendar departureDate){
		this.departureDate=departureDate;
	}
	/**
	 * 
	 * @param departureDate
	 * @return
	 */
	public GregorianCalendar getDepartureDate(GregorianCalendar departureDate){
		return departureDate;
	}
	/**
	 * 
	 * @param bill
	 */
	public void displayBill(Bill bill){
		room.displayRoom(room);
		client.displayClient(client);
		System.out.println("Date of arrival is : " + getDateOfArrival(dateOfArrival));
		System.out.println("Departure date is : " + getDepartureDate(departureDate));
	}
}
