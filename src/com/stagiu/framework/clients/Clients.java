package com.stagiu.framework.clients;

public class Clients {
	private String name;
	private Double CNP;
	private String loyalty;
	private int age;
	private String specialSituation;
	/*numar de tel*/
	/**
	 * 
	 * @param name "Name of the client"
	 * @param CNP	"Client's CNP"
	 * @param loyalty "If the client has some loyalty discount"
	 * @param age "Client's age"
	 * @param specialSituation "If they have any special situation like blind or dissabilities "
	 */
	public Clients(String name,Double CNP,String loyalty,int age,String specialSituation){
		this.name=name;
		this.CNP=CNP;
		this.loyalty=loyalty;
		this.age=age;		/*date of birth   
							Callendar raper peste date*/
		this.specialSituation=specialSituation;
		
	}
	/**
	 * 
	 * @param name
	 */
	public void setName(String name){
		this.name=name;
	}
	/**
	 * 
	 * @param name
	 * @return
	 */
	public String getName(String name){
		return name;
	}
	/**
	 * 
	 * @param CNP
	 */
	public void setCNP(Double CNP){
		this.CNP=CNP;
	}
	/**
	 * 
	 * @param CNP
	 * @return
	 */
	public Double getCNP(Double CNP){
		return CNP;
	}
	/**
	 * 
	 * @param loyalty
	 */
	public void  setLoyalty(String loyalty){
		this.loyalty=loyalty;
	}
	/**
	 * 
	 * @param loyalty
	 * @return
	 */
	public String getLoyalty(String loyalty){
		return loyalty;
	}
	/**
	 * 
	 * @param age
	 */
	public void setAge(int age){
		this.age=age;
	}
	/**
	 * 
	 * @param age
	 * @return
	 */
	public int getAge(int age){
		return age;
	}
	/**
	 * 
	 * @param specialSituation
	 */
	public void setSpecialSituation(String specialSituation){
		this.specialSituation=specialSituation;
	}
	/**
	 * 
	 * @param specialSituation
	 * @return
	 */
	public String getSpecialSituation(String specialSituation){
		return specialSituation;
	}
	public void displayClient(Clients client){
	System.out.println("Client's name is : "+ getName(name));
	System.out.println("Client's CNP is :" + getCNP(CNP));
	System.out.println("Client's age is :" +getAge(age));
	System.out.println("Client's type of loyalty : " +getLoyalty(loyalty));
	System.out.println("Client's special situaition : " + getSpecialSituation(specialSituation));
	}
	
}