package com.stagiu.framework.room;




/**
 * 
 * @author smiley
 * 
 */
public class Room {

	private int roomNumber;
/*	private String status; scos de peste tot si il integram in factura sau in rezervari  */ 
	private String endowments;
	private Double price;
	private int floor;

	/**
	 * 
	 * @param status
	 *            "The room can be Bussy Clear "
	 * @param price
	 *            "How much the room costs"
	 * @param floor
	 *            "What floor is the room located"
	 */
	public Room(String status, String endowments, Double price, int floor,int roomNumber) {
		
		this.endowments = endowments;
		this.price = price;
		this.floor = floor;
		this.roomNumber = roomNumber;
		System.out.println(Endowments.AC);
	}

	/**
	 * 
	 * @param price
	 * @return
	 */
	public void setPrice(Double price) {
		this.price = price;

	}

	/**
	 * 
	 * @param price
	 * @return
	 */
	public Double getPrice(Double price) {
		return price;
	}

	public int getRoom(int roomNumber) {
		return roomNumber;
	}

	/**
	 * 
	 * @param room
	 *            Display all informations about room
	 */
	public void displayRoom(Room room) {

		System.out.println("The room's number is " + getRoom(roomNumber));
		System.out.println("The room is at floor " + getFloor(floor));
		System.out.println("The room's price is " + getPrice(price));
		
		System.out.println("Endowments in the room "
				+ getEndowments(endowments));
	}

	/**
	 * 
	 * @param floor
	 */
	public void setfloor(int floor) {

		this.floor = floor;
	}

	public int getFloor(int floor) {
		return floor;
	}

	/**
	 * 
	 * @param roomNumber
	 */
	public void setRoom(int roomNumber) {
		// TODO Auto-generated method stub
		this.roomNumber = roomNumber;
	}
	/**
	 * 
	 * @param endowments
	 */
	public void setEndowments(String endowments) {
		// TODO Auto-generated method stub
		this.endowments = endowments;
	}

	public String getEndowments(String endowments) {
		return endowments;
	}
}