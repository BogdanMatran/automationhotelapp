package com.stagiu.framework.reservations;
import com.stagiu.framework.clients.Clients;
import com.stagiu.framework.room.Room;
import java.util.GregorianCalendar;
/**
 * 
 *	@author smiley
 *	client and room are class type 
 *	computerfield canal youtube
 *	
 *	
 *
 */

public class Reservations {
	private Room Room;/*va fi de tip Room clasa*/
	private Clients  Client;/*va fi de tip Client clasa  */
	private GregorianCalendar incomingDate;
	private GregorianCalendar leaveDate;
	/**
	 * 
	 * @param roomNumber
	 * @param nameClient
	 * @param incomingDate
	 * @param leaveDate
	 */
	public  Reservations(Room Room,Clients Client,GregorianCalendar incomingDate, GregorianCalendar leaveDate){
		this.Room=Room;
		this.Client=Client;
		this.incomingDate=incomingDate;
		this.leaveDate=leaveDate;		
	}
	
	
	
	
	
	void setIncomingDate(GregorianCalendar incomingDate){
		this.incomingDate=incomingDate;
	}
	/**
	 * 
	 * @param incomingDate
	 * @return
	 */
	GregorianCalendar getIncomingDate(GregorianCalendar incomingDate){
		return incomingDate;
	}
	/**
	 * 
	 * @param leaveDate
	 */
	void setLeaveDay(GregorianCalendar leaveDate){
		this.leaveDate=leaveDate;
	}
	/**
	 * 
	 * @param leaveDate
	 * @return
	 */
	GregorianCalendar getLeaveDate(GregorianCalendar leaveDate){
		return leaveDate;
	}
	/**
	 * 
	 * @param reservations
	 */
	void displayReservation(Reservations reservations){
		 Room.displayRoom(Room);
		 Client.displayClient(Client);
		System.out.println("Incoming date : " + getIncomingDate(incomingDate));
		System.out.println("Leave date : " + getLeaveDate(leaveDate));
	}
}
